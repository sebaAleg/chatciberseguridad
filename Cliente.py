import socket
import threading
import time
import Crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
from AES import AESCipher


class Cliente:
    def __init__(self):
        print("estoy vivo")

    def conectarServidor(self, direccionSv, puertoSv,protocolo):
        self.protocolo = protocolo
        self.direccionSv = direccionSv
        self.puertoSv = puertoSv
        if self.protocolo == "TCP":
            self.socket_cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket_cliente.connect((self.direccionSv, self.puertoSv))
        if self.protocolo == "UDP":
            print("Soy UDP")
            self.socket_cliente = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def iniciarChatTCP(self):
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                self.socket_cliente.send(mensaje)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje = str(self.socket_cliente.recv(1024))
                print("Recibido: "+mensaje)
                if mensaje == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()

    def iniciarChatTCPyRSA(self):
        self.chateando = True
        self.leeyendo = True
        #proceso para recibir clave, el servidor manda 3 mensajes en texto plano
        # el ultimo es el certificado
        var = []
        while True:
            mensaje = str(self.socket_cliente.recv(1024))
            print(mensaje)
            var.append(mensaje)
            if len(var) == 3:
                #Creo un cifrador, encripto un mensaje y lo mado al servidor
                clave_publica_servidor = var[2]
                self.llave_publica_servidor = RSA.importKey(clave_publica_servidor)
                cadena = "Esto es un test"
                cadena_bites = cadena.encode("utf-8")
                self.cifrador = PKCS1_OAEP.new(self.llave_publica_servidor)
                mensage_cifrado = self.cifrador.encrypt(cadena_bites)
                self.socket_cliente.send(mensage_cifrado)
                break
        # A estas altura ya tengo un mensaje cifrado mandado ahora generare las claves del cliente
        random_generator = Crypto.Random.new().read
        self.private_key = RSA.generate(1024, random_generator)
        self.public_key = self.private_key.publickey()
        public_key_utf8 = self.public_key.exportKey(format='PEM')
        self.socket_cliente.send(str(public_key_utf8))
        self.decifrador = PKCS1_OAEP.new(self.private_key)
        #Ahora tanto cliente como servidor tienen llaves publicas
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_codificado = mensaje.encode()
                mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
                #print("Encriptado" + str(mensage_encriptado))
                self.socket_cliente.send(mensage_encriptado)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje_recibido = self.socket_cliente.recv(1024)
                mensaje_decifrado = self.decifrador.decrypt(mensaje_recibido)
                print("Recibido: "+mensaje_decifrado)
                if mensaje_decifrado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()    

    def iniciarChatTCPyAES(self,clave,bits):
        # Defino la clave y bits del cifrado
        self.clave = clave
        self.bits = bits
        #se crea el encriptador
        encriptador = AESCipher(self.clave,self.bits)
        self.chateando = True
        self.leeyendo = True
        #recibo mensaje de bienvenida del servidor en texto plano
        mensaje = str(self.socket_cliente.recv(1024))
        print(mensaje)
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_encriptado = encriptador.encrypt(mensaje)
                self.socket_cliente.send(mensaje_encriptado)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje = str(self.socket_cliente.recv(1024))
                try:
                    mensaje_desencriptado = encriptador.decrypt(mensaje)
                except ValueError:
                    print(ValueError)
                    leeyendo()
                    break
                print("Recibido: "+mensaje_desencriptado)
                if mensaje_desencriptado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()

    def iniciarChatUDP(self):
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                self.socket_cliente.sendto(mensaje, (self.direccionSv, self.puertoSv))
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje = str(self.socket_cliente.recv(1024))
                print("Recibido: "+mensaje)
                if mensaje == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    
    def iniciarChatUDPyRSA(self):
        self.chateando = True
        self.leeyendo = True
        # Mandando mensaje de coneccion 
        mensaje_de_saludo = self.socket_cliente.sendto("Hola Servidor",(self.direccionSv, self.puertoSv))
        #Capturar respuesta del servidor
        mensaje_de_respuesta = str(self.socket_cliente.recv(1024))
        print(mensaje_de_respuesta)
        # Creando claves criptograficas
        random_generator = Crypto.Random.new().read
        self.private_key = RSA.generate(1024, random_generator)
        self.public_key = self.private_key.publickey()
        public_key_utf8 = self.public_key.exportKey(format='PEM')
        # mandando clave
        self.socket_cliente.sendto(public_key_utf8,(self.direccionSv, self.puertoSv))
        # reciviendo mensaje encriptado
        mensaje_de_respuesta = self.socket_cliente.recv(1024)
        print("Mensaje encriptado recivido: "+ str(mensaje_de_respuesta))
        #desencriptandolo y mandando de vuelta
        self.decifrador = PKCS1_OAEP.new(self.private_key)
        mensaje_decifrado = self.decifrador.decrypt(mensaje_de_respuesta)
        print("Mensaje decifrado: "+ str(mensaje_decifrado))
        self.socket_cliente.sendto(mensaje_decifrado,(self.direccionSv,self.puertoSv))
        #Recibiendo clave publica del servidor
        llave_publica_servidor = self.socket_cliente.recv(1024)
        # Creando instancias para cifrar
        self.llave_publica_servidor = RSA.importKey(llave_publica_servidor)
        self.cifrador = PKCS1_OAEP.new(self.llave_publica_servidor)
        # Cifrando mensaje como prueba
        #mandando dato cifrado
        print("[No e mandado clave publica al al servidor ] Cifrando mensaje y mandandolo")
        mensaje = "test del cliente"
        mensaje_codificado = mensaje.encode()
        mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
        print("Encriptado: " + str(mensage_encriptado))
        self.socket_cliente.sendto(mensage_encriptado,(self.direccionSv,self.puertoSv))

        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_codificado = mensaje.encode()
                mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
                self.socket_cliente.sendto(mensage_encriptado, (self.direccionSv, self.puertoSv))
                #print("Encriptado: " + str(mensage_encriptado))
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje = self.socket_cliente.recv(1024)
                mensaje_decifrado = self.decifrador.decrypt(mensaje)
                print("Recibido: "+mensaje_decifrado)
                if mensaje_decifrado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    def iniciarChatUDPyAES(self,clave,bits):
        #mandamos mensaje de bienvenida en texto plano
        mensaje_bienvenida = "Hola servidor"
        self.socket_cliente.sendto(mensaje_bienvenida, (self.direccionSv, self.puertoSv))
        #recibimos un mensaje del servidor, suponiendo que este captura nuestra direccion
        mensaje = str(self.socket_cliente.recv(1024))
        print(mensaje)
        #instanciamos claves y bits
        self.clave = clave
        self.bits = bits
        #ahora creamos el encriptador
        encriptador = AESCipher(self.clave,self.bits)

        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_encriptado = encriptador.encrypt(mensaje)
                self.socket_cliente.sendto(mensaje_encriptado, (self.direccionSv, self.puertoSv))
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while self.leeyendo:
                mensaje = str(self.socket_cliente.recv(1024))
                mensaje_desencriptado = encriptador.decrypt(mensaje)
                print("Recibido: "+mensaje_desencriptado)
                if mensaje_desencriptado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socket_cliente.close()
                    break              
        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    
    def testUDP(self):
        while True:
                mensaje = str(raw_input(">>"))
                self.socket_cliente.sendto(mensaje, (self.direccionSv, self.puertoSv))
                mensaje = self.socket_cliente.recv(1024)
                print(mensaje)
                if mensaje == "adios":
                    #print(str(self.chateando))
                    break


               
        

            