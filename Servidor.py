# coding=utf-8
import socket
import threading
import Crypto
from Crypto.PublicKey import RSA
import binascii
import time
from Crypto.Cipher import PKCS1_OAEP
from AES import AESCipher


class Servidor:
    def __init__(self, direccionSv, puertoSv, orejas, protocolo):
        self.protocolo = protocolo
        self.direccionSv = direccionSv
        self.puertoSv = puertoSv
        self.orejas = orejas
        server_address = (self.direccionSv, self.puertoSv)
        if protocolo == "TCP":
            self.socketServidor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socketServidor.bind(server_address)
            self.socketServidor.listen(self.orejas)
        if protocolo == "UDP":
            self.socketServidor = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socketServidor.bind(server_address)

            

    def chatConTCP(self,):
        print("iniciando conecciones")
        coneccion, direccion = self.socketServidor.accept()
        print("cliente conectado de "+str(direccion))
        coneccion.send("Hola cliente, soy el servidor")
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                coneccion.send(mensaje)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                mensaje = str(coneccion.recv(1024))
                print("Recibido: "+mensaje)
                if mensaje == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    coneccion.close()
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    def chatConUDP(self):
        print("iniciando conecciones")
        data, addr = self.socketServidor.recvfrom(1024)
        self.direccionCliente = addr
        print(data)
        mensaje = "capture tu direccion :)"
        self.socketServidor.sendto(mensaje, self.direccionCliente)
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                self.socketServidor.sendto(mensaje, self.direccionCliente)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                data, addr = self.socketServidor.recvfrom(1024)
                mensaje = str(data)
                print("Recibido: "+mensaje)
                if mensaje == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()

    def chatConTCPyRSA(self,):
        print("iniciando conecciones")
        coneccion, direccion = self.socketServidor.accept()
        #Encriptando
        random_generator = Crypto.Random.new().read
        self.private_key = RSA.generate(1024, random_generator)
        self.public_key = self.private_key.publickey()
        public_key_utf8 = self.public_key.exportKey(format='PEM')
        print("cliente conectado de "+str(direccion))
        coneccion.send("Hola cliente, soy el servidor")
        time.sleep(1)
        coneccion.send("Por favor recive esta clave publica de mi parte")
        time.sleep(1)
        coneccion.send(str(public_key_utf8))
        self.decifrador = PKCS1_OAEP.new(self.private_key)
        #proceso para recivir la clave
        #   Aqui recivo el mensaje de prueba
        mensaje_recibido = coneccion.recv(1024)
        mensaje_decifrado = self.decifrador.decrypt(mensaje_recibido)
        print("Recibido: "+mensaje_decifrado)
        # Luego espero recivir la clave publica del cliente
        clave_esperada = coneccion.recv(1024)
        self.llave_publica_cliente = RSA.importKey(clave_esperada)
        self.cifrador = PKCS1_OAEP.new(self.llave_publica_cliente)
        print("recivi del cliente el siguiente certificado")
        print(str(clave_esperada))


        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_codificado = mensaje.encode()
                mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
                #print("Encriptado: " + str(mensage_encriptado))
                coneccion.send(mensage_encriptado)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                mensaje_recibido = coneccion.recv(1024)
                mensaje_decifrado = self.decifrador.decrypt(mensaje_recibido)
                print("Recibido: "+mensaje_decifrado)
                if mensaje_decifrado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    coneccion.close()
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    def chatConUDPyRSA(self):
        print("iniciando conecciones")
        data, addr = self.socketServidor.recvfrom(1024)
        self.direccionCliente = addr
        print(data)
        mensaje = "capture tu direccion :)"
        self.socketServidor.sendto(mensaje, self.direccionCliente)
        # recibiendo clave publica del cliente
        clave_publica_cliente = self.socketServidor.recvfrom(1024)[0]
        print(clave_publica_cliente)
        # Creando instancias para cifrar
        self.llave_publica_cliente = RSA.importKey(clave_publica_cliente)
        self.cifrador = PKCS1_OAEP.new(self.llave_publica_cliente)
        #mandando dato cifrado
        print("[No e mandado clave publica al cliente ] Cifrando mensaje y mandandolo")
        mensaje = "Test"
        mensaje_codificado = mensaje.encode()
        mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
        print("Encriptado: " + str(mensage_encriptado))
        self.socketServidor.sendto(mensage_encriptado,self.direccionCliente)
        #reciviendo mensaje decifrado como respuesta
        mensaje_decifrado = self.socketServidor.recvfrom(1024)[0]
        print("Recibido del cliente: "+ str(mensaje_decifrado))
        #creando claves del servidor
        random_generator = Crypto.Random.new().read
        self.private_key = RSA.generate(1024, random_generator)
        self.public_key = self.private_key.publickey()
        public_key_utf8 = self.public_key.exportKey(format='PEM')
        #mandando clave publica para el cliente
        self.socketServidor.sendto(public_key_utf8,self.direccionCliente)
        #Recibiendo mensaje de test del cliente
        mensaje_cifrado = self.socketServidor.recvfrom(1024)[0]
        #desencriptando mensaje del cliente
        self.decifrador = PKCS1_OAEP.new(self.private_key)
        mensaje_decifrado = self.decifrador.decrypt(mensaje_cifrado)
        print("Mensaje decifrado: "+ str(mensaje_decifrado))
        ## LLegado a este punto tanto cliente como servidor tienen sus respectivas claves publicas
        ## y privadas

        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_codificado = mensaje.encode()
                mensage_encriptado = self.cifrador.encrypt(mensaje_codificado)
                #print("Encriptado: " + str(mensage_encriptado))
                self.socketServidor.sendto(mensage_encriptado, self.direccionCliente)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                data, addr = self.socketServidor.recvfrom(1024)
                mensaje_decifrado = self.decifrador.decrypt(data)
                print("Recibido: "+mensaje_decifrado)
                if mensaje_decifrado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()

    def chatConTCPyAES(self,clave,bits):
        # Defino clave con la que se encriptara el mensaje y la cantidad de btis (128 o 256)
        self.clave = clave
        self.bits = bits
        print(self.bits)
        #espero la coneccion 
        print("iniciando conecciones")
        coneccion, direccion = self.socketServidor.accept()
        print("cliente conectado de "+str(direccion))
        #mandamos una coneccion en texto plano para probar
        coneccion.send("Hola cliente, soy el servidor")
        # Comienzo el cifrado
        encriptador = AESCipher(self.clave,self.bits)
        print(len(encriptador.key))
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_encriptado = encriptador.encrypt(mensaje)
                coneccion.send(mensaje_encriptado)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                mensaje = str(coneccion.recv(1024))
                try:
                    mensaje_desencriptado = encriptador.decrypt(mensaje)
                except ValueError:
                    print(ValueError)
                
                print("Recibido: "+mensaje_desencriptado)
                if mensaje_desencriptado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    coneccion.close()
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()
    def chatConUDPyAES(self,clave,bits):
        #Defino claves para cifrar y bits decirfrado
        self.clave=clave
        self.bits=bits
        #creo el encriptador
        encriptador = AESCipher(self.clave,self.bits)
        print("iniciando conecciones")
        data, addr = self.socketServidor.recvfrom(1024)
        self.direccionCliente = addr
        print(data)
        mensaje = "capture tu direccion :)"
        self.socketServidor.sendto(mensaje, self.direccionCliente)
        self.chateando = True
        self.leeyendo = True
        def escribiendo():
            while self.chateando:
                mensaje = str(raw_input(">>"))
                mensaje_encriptado = encriptador.encrypt(mensaje)
                self.socketServidor.sendto(mensaje_encriptado, self.direccionCliente)
                if mensaje == "adios":
                    self.chateando = False
                    print(str(self.chateando))
                    break
        def leeyendo():
            while leeyendo:
                data, addr = self.socketServidor.recvfrom(1024)
                mensaje = str(data)
                mensaje_desencriptado = encriptador.decrypt(mensaje)
                print("Recibido: "+mensaje_desencriptado)
                if mensaje_desencriptado == "adios":
                    self.leeyendo = False
                    print(str(self.leeyendo))
                    break
        def cerrando():
            while True:
                if self.chateando == False and self.leeyendo == False:
                    print("cerrando puertos")
                    self.socketServidor.close()
                    break  

        hilo = threading.Thread(target=escribiendo)
        hilo2 = threading.Thread(target=leeyendo)
        hilo3 = threading.Thread(target=cerrando)
        hilo2.start()
        hilo.start()
        hilo3.start()

    def testUDP(self):
        while True:
            data, addr = self.socketServidor.recvfrom(1024)
            print("iniciando conecciones")
            ip,puerto = addr
            print(str(data))
            print(str(ip)+" "+str(puerto))
            mensaje=str(raw_input(">>"))
            self.socketServidor.sendto(mensaje,addr)
            if str(data) == "adios":
                break
            

