# -*- coding: utf-8 -*-
import sys
from Cliente import Cliente

try:
     puerto = int(raw_input("Ingrese puerto de coneccion al servidor el servidor: "))
except ValueError:
    print("Error, introduce un entero")
    print(ValueError)
    sys.exit()
try:
    direccion = str(raw_input("ingrese direccion de ip del servidor: "))
except ValueError:
    print("ip mal ingresada ip:EJ 123.123.123 o localhost")
print("El puerto a conectarse es el "+str(puerto))
print("###################################")
print("Seleccione protocolo")
print("1.-TCP")
print("2.-UDP")
try:
     protocolo = int(raw_input())
except ValueError:
      print("Error, introduce un entero")
      print(ValueError)
      sys.exit()
if protocolo == 1:
     cliente1 = Cliente()
     # Ahora preguntamos cual encriptacion desea
     print("Por favor indique que encriptación desea")
     print("1.-Simetrica")
     print("2.-Asimetrica")
     try:
          respuesta = int(raw_input())
     except ValueError:
          print("Error introduce un entero")
          sys.exit()
     if respuesta == 1:
          #Preguntamos si quiere AES 128 o 256
          print("Seleccione tipo de cifrado simetrico")
          print("1.-AES 128")
          print("2.-AES 256")
          try:
               respuesta = int(raw_input())
          except ValueError:
               print("Error introduce un entero")
               sys.exit()
          if respuesta == 1:
               print("Elija una contraseña para cifrar en AES 128")
               respuesta = str(raw_input())
               print("Creando Cliente TCP con AES 128")
               cliente1.conectarServidor(direccion,puerto,"TCP")
               cliente1.iniciarChatTCPyAES(respuesta,128)
          if respuesta == 2:
               print("Elija una contraseña para cifrar en AES 256")
               respuesta = str(raw_input())
               print("Creando Cliente TCP con AES 256")
               cliente1.conectarServidor(direccion,puerto,"TCP")
               cliente1.iniciarChatTCPyAES(respuesta,256)
     if respuesta == 2:
          print("Creando Cliente TCP con RSA")
          cliente1.conectarServidor(direccion,puerto,"TCP")                         
          cliente1.iniciarChatTCPyRSA()
if protocolo == 2:
     cliente1 = Cliente()
     # Ahora preguntamos cual encriptacion desea
     print("Por favor indique que encriptación desea")
     print("1.-Simetrica")
     print("2.-Asimetrica")
     try:
          respuesta = int(raw_input())
     except ValueError:
          print("Error introduce un entero")
          sys.exit()
     if respuesta == 1:
          #Preguntamos si quiere AES 128 o 256
          print("Seleccione tipo de cifrado simetrico")
          print("1.-AES 128")
          print("2.-AES 256")
          try:
               respuesta = int(raw_input())
          except ValueError:
               print("Error introduce un entero")
               sys.exit()
          if respuesta == 1:
               print("Elija una contraseña para cifrar en AES 128")
               respuesta = str(raw_input())
               print("Creando Cliente UDP con AES 128")
               cliente1.conectarServidor(direccion,puerto,"UDP")
               cliente1.iniciarChatUDPyAES(respuesta,128)
          if respuesta == 2:
               print("Elija una contraseña para cifrar en AES 256")
               respuesta = str(raw_input())
               print("Creando Cliente UDP con AES 256")
               cliente1.conectarServidor(direccion,puerto,"UDP")
               cliente1.iniciarChatUDPyAES(respuesta,256)
     if respuesta == 2:
          print("Creando Cliente UDP con RSA")
          cliente1.conectarServidor(direccion,puerto,"UDP")                         
          cliente1.iniciarChatUDPyRSA()
