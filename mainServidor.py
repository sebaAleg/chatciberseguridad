# -*- coding: utf-8 -*-
from Servidor import Servidor
import sys
import threading
import time
try:
    puerto = int(raw_input("Ingrese puerto donde se abrira el servidor: "))
except ValueError:
    print("Error, introduce un entero")
    print(ValueError)
    sys.exit()

try:
    direccion = str(raw_input("ingrese direccion de ip del servidor: "))
except ValueError:
    print("ip mal ingresada ip:EJ 123.123.123 o localhost")
print("su puerto es el "+str(puerto))
print("############################")
print("¿ Que protocolo usaras ?")
print("1.-TCP")
print("2.-UDP")
try:
    protocolo=int(raw_input())
except ValueError:
    print("Error, introduce un entero")
    print(ValueError)
    sys.exit()
if protocolo == 1:
    
    # Ahora preguntamos cual encriptacion desea
    print("Por favor indique que encriptación desea")
    print("1.-Simetrica")
    print("2.-Asimetrica")
    try:
        respuesta = int(raw_input())
    except ValueError:
        print("Error introduce un entero")
        sys.exit()
    if respuesta == 1:
        #Preguntamos si quiere AES 128 o 256
        print("Seleccione tipo de cifrado simetrico")
        print("1.-AES 128")
        print("2.-AES 256")
        try:
            respuesta = int(raw_input())
        except ValueError:
            print("Error introduce un entero")
            sys.exit()
        if respuesta == 1:
            print("Elija una contraseña para cifrar en AES 128")
            respuesta = str(raw_input())
            print("Creando Cliente TCP con AES 128")
            servidor = Servidor(direccion, int(puerto), 2,"TCP")
            servidor.chatConTCPyAES(respuesta,128)
        if respuesta == 2:
            print("Elija una contraseña para cifrar en AES 256")
            respuesta = str(raw_input())
            print("Creando Cliente TCP con AES 256")
            servidor = Servidor(direccion, int(puerto), 2,"TCP")
            servidor.chatConTCPyAES(respuesta,256)
    if respuesta == 2:
        print("Creando Cliente TCP con RSA")
        servidor = Servidor(direccion, int(puerto), 2,"TCP")                         
        servidor.chatConTCPyRSA()
if protocolo == 2:
    print("UDP")
    # Ahora preguntamos cual encriptacion desea
    print("Por favor indique que encriptación desea")
    print("1.-Simetrica")
    print("2.-Asimetrica")
    try:
        respuesta = int(raw_input())
    except ValueError:
        print("Error introduce un entero")
        sys.exit()
    if respuesta == 1:
        #Preguntamos si quiere AES 128 o 256
        print("Seleccione tipo de cifrado simetrico")
        print("1.-AES 128")
        print("2.-AES 256")
        try:
            respuesta = int(raw_input())
        except ValueError:
            print("Error introduce un entero")
            sys.exit()
        if respuesta == 1:
            print("Elija una contraseña para cifrar en AES 128")
            respuesta = str(raw_input())
            print("Creando Cliente UDP con AES 128")
            servidor = Servidor(direccion, int(puerto), 2,"UDP")
            servidor.chatConUDPyAES(respuesta,128)
        if respuesta == 2:
            print("Elija una contraseña para cifrar en AES 256")
            respuesta = str(raw_input())
            print("Creando Cliente UDP con AES 256")
            servidor = Servidor(direccion, int(puerto), 2,"UDP")
            servidor.chatConUDPyAES(respuesta,256)

    if respuesta == 2:
        print("Creando Cliente UDP con RSA")
        servidor = Servidor(direccion, int(puerto), 2,"UDP")                         
        servidor.chatConUDPyRSA()
    #servidor1.chatConUDPyAES("wow123",128)
    #servidor1.chatConUDP()
    #servidor1.esperarConUDP()
    #servidor1.empezarMensajeria()